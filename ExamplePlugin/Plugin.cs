﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrinterHandler
{
    public class Plugin : IPlugin
    {
        public Plugin()
        {

        }

        public void dllEntry(IMessage message)
        {
            switch(message.getID())
            {
                case Catalog.DO_SOMETHING:
                    {
                        try
                        {
                            String str = (String)message.getData();
                            doSomething(str);
                        }
                        catch(Exception e)
                        {
                            Log.l(e.Message);
                        }
                        break;
                    }
                case Catalog.DO_SOMETHING_DIFFERENT:
                    {
                        try
                        {
                            String [] strings = (String [])message.getData();
                            doSomethingDifferent(strings);
                        }
                        catch (Exception e)
                        {
                            Log.l(e.Message);
                        }
                        break;
                    }
            }
        }


        private void doSomething(String example)
        {
            Log.l(example);
        }

        private void doSomethingDifferent(String [] stringZ)
        {
            //do something different
        }
    }
}
