﻿using PrinterHandler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilePlugin
{
    class Catalog
    {
        class FolderCreateMessage : IMessage
        {
            private const int ID = 5;
            private StringandNum message;

            public FolderCreateMessage(StringandNum obj)
            {
                message = obj;
            }

            public int getID()
            {
                return ID;
            }

            public Object getData()
            {
                return message;
            }
        }

        class FolderCreateSentenceMessege : IMessage
        {
            private const int ID = 6;
            private String message;

            public FolderCreateSentenceMessege(String str)
            {
                message = str;
            }

            public int getID()
            {
                return ID;
            }

            public Object getData()
            {
                return message;
            }
        }

        public struct StringandNum
        {
            public StringandNum(String str, int num)
            {
                this.num = num;
                this.str = str;
            }
            public int num;
            public String str;
        }
    }
}
