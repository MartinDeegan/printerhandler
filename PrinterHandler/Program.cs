﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace PrinterHandler
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        ///
        static void Main(String [] args)
        {
#if DEBUG
            if (Environment.UserInteractive)
            {
                PrinterHandler service = new PrinterHandler();
                service.TestStartupAndStop(args);
            }
#endif
#if RELEASE
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
               new PrinterHandler() 
            };
            ServiceBase.Run(ServicesToRun);
#endif
        }
    }
}
