﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using PrinterHandler.Networking;

namespace PrinterHandler
{
    public partial class PrinterHandler : ServiceBase
    {
        public PrinterHandler()
        {
            InitializeComponent();
#if DEBUG
           
#endif
        }

        protected override void OnStart(string[] args)
        {
            Log.initLogging();
            Log.m(Security.FingerPrint.Value());
            CommandLoop.start();
            PluginLoader.start();
            Connection.start(CommandLoop.addCommand);
            CommandLoop.addCommand(new ExampleMessage("This is the Example Message."));
            CommandLoop.addCommand(new ExampleMessage("Example message 2."));
            CommandLoop.addCommand(new ExampleMessage("This is the third Example Message."));
            //CommandLoop.addCommand(new ShutdownMessage("Ur gey"));
            //CommandLoop.addCommand(new RestartMessage("Cya mate xD"));
        }

        protected override void OnStop()
        {
            Log.l("On Stop");
            stopEverything();
        }

        public static void stopEverything()
        {
            Log.l("Stopping everything");
            Connection.stop();
            CommandLoop.stop();
            Log.stopLogging();
        }

#if DEBUG
        internal void TestStartupAndStop(String [] args)
        {
            OnStart(args);
            Console.ReadLine();
            OnStop();
        }
#endif
    }
}
