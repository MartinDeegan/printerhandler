﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace PrinterHandler
{
    class CommandLoop
    {
        private static List<JObject> commands = new List<JObject>();

        private const int LOOP_LENGTH_ACTIVE = 1000;

        private static bool running = false;

        private static System.Timers.Timer commandLoopTimer;

        public static void addCommand(JObject message)
        {
            if (running)
            {
                commands.Add(message);
            }
        }

        private static void run(Object source, ElapsedEventArgs e)
        {

            Log.l("Running command loop");
            if (commands.Count > 0)
            {
                List<IPlugin> plugins = PluginLoader.getLoadedPlugins();
                foreach (IPlugin plugin in plugins)
                {
                    plugin.dllEntry(commands.ToArray()[0]);
                }
                commands.RemoveAt(0);
            }

        }

        public static void start()
        {
            Log.l("Starting command loop");
            running = true;
            commandLoopTimer.Start();            
        }

        static CommandLoop()
        {
            Log.l("Init command loop");
            commandLoopTimer = new System.Timers.Timer();
            commandLoopTimer.Elapsed += run;
            commandLoopTimer.Interval = LOOP_LENGTH_ACTIVE;
        }

        public static void stop()
        {
            Log.m("stop command loop");
            commandLoopTimer.Stop();
            running = false;
            for (int i = 0; i < commands.Count; i++)
            {
                run(null, null);
            }
            commands = null;
            Log.l("finish stop command loop");
            PluginLoader.detatchPlugins();
        }
    }
}
