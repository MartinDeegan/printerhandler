﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrinterHandler
{
    class ExampleMessage : IMessage
    {
        private const int ID = 1000;
        private String message;

        public int getID()
        {
            return ID;
        }

        public Object getData()
        {
            Object data = message;
            return data;
        }

        public ExampleMessage(String data)
        {
            message = data;
        }
    }

    class ShutdownMessage : IMessage
    {
        private const int ID = 0;
        private String message;

        public ShutdownMessage(String str)
        {
            message = str;
        }

        public int getID()
        {
            return ID;
        }

        public Object getData()
        {
            return message;
        }
    }

    class RestartMessage : IMessage
    {
        private const int ID = 1;
        private String message;

        public RestartMessage(String str)
        {
            message = str;
        }

        public int getID()
        {
            return ID;
        }

        public Object getData()
        {
            return message;
        }
    }

    class FolderCreateMessage : IMessage
    {
        private const int ID = 5;
        private StringandNum message;

        public FolderCreateMessage(StringandNum obj)
        {
            message = obj;
        }

        public int getID()
        {
            return ID;
        }

        public Object getData()
        {
            return message;
        }
    }

    class FolderCreateSentenceMessege : IMessage
    {
        private const int ID = 6;
        private String message;

        public FolderCreateSentenceMessege(String str)
        {
            message = str;
        }

        public int getID()
        {
            return ID;
        }

        public Object getData()
        {
            return message;
        }
    }

    public struct StringandNum
    {
        public StringandNum(String str, int num)
        {
            this.num = num;
            this.str = str;
        }
        public int num;
        public String str;
    }
}
