﻿using PrinterHandler;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PrinterHandler
{
    
    class PluginLoader
    {
        private static List<IPlugin> plugins = new List<IPlugin>();
        private static bool hasLoadedPlugins = false;

        static PluginLoader()
        {

        }

        public static void start()
        {
            Log.l("Init Plugin loader");
            String directory = AppDomain.CurrentDomain.BaseDirectory + "\\Plugins";
            String[] dlls = Directory.GetFiles(directory, "*.dll");
            foreach (String dllPath in dlls)
            {
                var loadedDll = Assembly.LoadFrom(dllPath);
                foreach (Type type in loadedDll.GetExportedTypes())
                {
                    if (type.FullName.EndsWith("Plugin"))
                    {
                        try
                        {
                            dynamic loadedClass = Activator.CreateInstance(type);
                            IPlugin plugin = loadedClass;
                            plugins.Add(plugin);
                        }
                        catch(Exception e)
                        {
                            Log.e(e.Message);
                        }
                    }
                }
            }
            hasLoadedPlugins = true;
        }

        public static List<IPlugin> getLoadedPlugins()
        {
            return plugins;
        }

        public static bool hasLoaded()
        {
            return hasLoadedPlugins;
        }

        public static void detatchPlugins()
        {
            Log.l("detatch plugins");
            plugins = null;
            Log.l("finish detatch plugins");
        }
    }
}
