var dgram = require('dgram');
var server = dgram.createSocket('udp4');

var HOST = '104.131.122.59';
var PORT = 6969;

server.on('listening', function() {
    var address = server.address();
    console.log('UDP Server listening on ' + address.address + ":" + address.port);
});

server.on('message', function(message, remote) {
    console.log(remote.address + ':' + remote.port + ' - ' + message);

    var message = new Object();
    message.ID = 1000;
    message.Data = "Hello, this is the server";

    var toSend = new Buffer(JSON.stringify(message));
    server.send(toSend, 0, toSend.length, remote.port, remote.address, function(err) {
        console.log(err);
    });
});

server.on('error', function(err) {
    if(err)
    {
        console.log(err.message);
    }
});

server.on('close', function() { 
    console.log("Closing");
});

server.bind(PORT, HOST);