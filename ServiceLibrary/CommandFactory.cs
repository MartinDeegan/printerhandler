﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrinterHandler
{
    class CommandFactory
    {
        public static IMessage createMessage(int ID, JToken Value)
        {
            Object Data = Value.ToObject<Object>();
            return new BaseMessage(ID,Data);
        }
    }

    class BaseMessage : IMessage
    {
        public BaseMessage(int id, Object data)
        {
            ID = id;
            Data = data;
        }

        public int ID;
        public Object Data;

        public int getID()
        {
            return ID;
        }

        public Object getData()
        {
            return Data;
        }
    }
}
