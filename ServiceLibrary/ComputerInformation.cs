﻿using System;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Security;
using System.Collections;

namespace PrinterHandler.Networking
{
	class ComputerInformationJsonObject
	{
		public ComputerInformationJsonObject()
		{
			id = Security.FingerPrint.Value();
			computername = Environment.MachineName;
			Version ver = Assembly.GetEntryAssembly().GetName().Version;
			version = ver.ToString();
		}

		public String id;
		public String computername;
		public String version;
		public bool available;
	}
}


namespace Security
{
	/// <summary>
	/// Generates a 16 byte Unique Identification code of a computer
	/// Example: 4876-8DB5-EE85-69D3-FE52-8CF7-395D-2EA9
	/// </summary>
	public class FingerPrint  
	{
		private static String fingerPrint = String.Empty;
		public static String Value()
		{
			if (String.IsNullOrEmpty(fingerPrint))
			{
				fingerPrint = GetHash("CPU >> " + cpuId() + "\nBIOS >> " + 
				biosId() + "\nBASE >> " + baseId());
			}
			return fingerPrint;
		}
		private static String GetHash(String s)
		{
			MD5 sec = new MD5CryptoServiceProvider();
			ASCIIEncoding enc = new ASCIIEncoding();
			byte[] bt = enc.GetBytes(s);
			return GetHexString(sec.ComputeHash(bt));
		}
		private static String GetHexString(byte[] bt)
		{
			String s = String.Empty;
			for (int i = 0; i < bt.Length; i++)
			{
				byte b = bt[i];
				int n, n1, n2;
				n = (int)b;
				n1 = n & 15;
				n2 = (n >> 4) & 15;
				if (n2 > 9)
					s += ((char)(n2 - 10 + (int)'A')).ToString();
				else
					s += n2.ToString();
				if (n1 > 9)
					s += ((char)(n1 - 10 + (int)'A')).ToString();
				else
					s += n1.ToString();
				if ((i + 1) != bt.Length && (i + 1) % 2 == 0) s += "-";
			}
			return s;
		}
		#region Original Device ID Getting Code
		//Return a hardware identifier
		private static String identifier
		(String wmiClass, String wmiProperty, String wmiMustBeTrue)
		{
			String result = "";
			System.Management.ManagementClass mc = 
		new System.Management.ManagementClass(wmiClass);
			System.Management.ManagementObjectCollection moc = mc.GetInstances();
			foreach (System.Management.ManagementObject mo in moc)
			{
				if (mo[wmiMustBeTrue].ToString() == "True")
				{
					//Only get the first one
					if (result == "")
					{
						try
						{
							result = mo[wmiProperty].ToString();
							break;
						}
						catch
						{
						}
					}
				}
			}
			return result;
		}
		//Return a hardware identifier
		private static String identifier(String wmiClass, String wmiProperty)
		{
			String result = "";
			System.Management.ManagementClass mc = 
		new System.Management.ManagementClass(wmiClass);
			System.Management.ManagementObjectCollection moc = mc.GetInstances();
			foreach (System.Management.ManagementObject mo in moc)
			{
				//Only get the first one
				if (result == "")
				{
					try
					{
						result = mo[wmiProperty].ToString();
						break;
					}
					catch
					{
					}
				}
			}
			return result;
		}
		private static String cpuId()
		{
			//Uses first CPU identifier available in order of preference
			//Don't get all identifiers, as it is very time consuming
			String retVal = identifier("Win32_Processor", "UniqueId");
			if (retVal == "") //If no UniqueID, use ProcessorID
			{
				retVal = identifier("Win32_Processor", "ProcessorId");
				if (retVal == "") //If no ProcessorId, use Name
				{
					retVal = identifier("Win32_Processor", "Name");
					if (retVal == "") //If no Name, use Manufacturer
					{
						retVal = identifier("Win32_Processor", "Manufacturer");
					}
					//Add clock speed for extra security
					retVal += identifier("Win32_Processor", "MaxClockSpeed");
				}
			}
			return retVal;
		}
		//BIOS Identifier
		private static String biosId()
		{
			return identifier("Win32_BIOS", "Manufacturer")
			+ identifier("Win32_BIOS", "SMBIOSBIOSVersion")
			+ identifier("Win32_BIOS", "IdentificationCode")
			+ identifier("Win32_BIOS", "SerialNumber")
			+ identifier("Win32_BIOS", "ReleaseDate")
			+ identifier("Win32_BIOS", "Version");
		}
		//Main physical hard drive ID
		private static String diskId()
		{
			return identifier("Win32_DiskDrive", "Model")
			+ identifier("Win32_DiskDrive", "Manufacturer")
			+ identifier("Win32_DiskDrive", "Signature")
			+ identifier("Win32_DiskDrive", "TotalHeads");
		}
		//Motherboard ID
		private static String baseId()
		{
			return identifier("Win32_BaseBoard", "Model")
			+ identifier("Win32_BaseBoard", "Manufacturer")
			+ identifier("Win32_BaseBoard", "Name")
			+ identifier("Win32_BaseBoard", "SerialNumber");
		}
		//Primary video controller ID
		private static String videoId()
		{
			return identifier("Win32_VideoController", "DriverVersion")
			+ identifier("Win32_VideoController", "Name");
		}
		//First enabled network card ID
		private static String macId()
		{
			return identifier("Win32_NetworkAdapterConfiguration", 
				"MACAddress", "IPEnabled");
		}
		#endregion
	}
}

