﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Threading;

namespace PrinterHandler.Networking
{
    public class Connection
    {
        private const int PORT = 6969;
        private const String IP = "104.131.122.59";
        private static UdpClient client;

        private static IAsyncResult recieveResult;

        private static IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, PORT);
        private static IPEndPoint remoteEndPoint = new IPEndPoint(IPAddress.Parse(IP), PORT);

        public static bool isAvailable = false;

        private static ComputerInformationJsonObject computerInformation;
        private static System.Timers.Timer keepOpenTimer;

        public delegate void addCommandFunction(IMessage message);
        public static addCommandFunction addCommand;

        private static Thread connectionThread;

        private const int UPDATE_TIME = 10000;

        private static void OnRecieve(IAsyncResult asyncResult)
        {
            IPEndPoint endPoint = (IPEndPoint)asyncResult.AsyncState;
            Byte[] receiveBytes = client.EndReceive(asyncResult, ref endPoint);
            String receiveString = Encoding.ASCII.GetString(receiveBytes);
            Log.m(receiveString);
            
            try
            {
                JObject recievedObj = JObject.Parse(receiveString);
                addCommand(CommandFactory.createMessage((int)recievedObj["ID"], recievedObj["Data"]));
            }
            catch(Exception e)
            {
                Log.e(e.Message);
            }
            
            startRecieving();
        }

        private static void startRecieving()
        {           
            Log.l("Listening for messages");
            recieveResult = client.BeginReceive(new AsyncCallback(OnRecieve), localEndPoint);
        }

        private static void OnSend(IAsyncResult asyncResult)
        {
            client.EndSend(asyncResult);
        }

        private static void connectToServer()
        {
            client = new UdpClient(localEndPoint);
            client.Connect(remoteEndPoint);

            startRecieving();

            computerInformation = new ComputerInformationJsonObject();
            computerInformation.available = isAvailable;
            String computerJson = JsonConvert.SerializeObject(computerInformation);
            Log.m(computerJson);
            String toSend = computerJson;
            Byte[] data = Encoding.ASCII.GetBytes(toSend);
            send(data);
            keepOpenTimer = new System.Timers.Timer();
            keepOpenTimer.Elapsed += new ElapsedEventHandler(onUpdateServerEvent);
            keepOpenTimer.Interval = UPDATE_TIME;
        }

        private static void onUpdateServerEvent(Object source, ElapsedEventArgs e)
        {
            computerInformation = new ComputerInformationJsonObject();
            computerInformation.available = isAvailable;
            String computerJson = JsonConvert.SerializeObject(computerInformation);
            String toSend = computerJson;
            Byte[] data = Encoding.ASCII.GetBytes(toSend);
            send(data);
        }

        public static IAsyncResult send(Byte [] data)
        {
            Log.l("Sending data");
            return client.BeginSend(data, data.Length, new AsyncCallback(OnSend), null);
        }

        public static void start(addCommandFunction loopFunction)
        {
            addCommand = loopFunction;
            isAvailable = true;
            connectToServer();
        }

        public static void stop()
        {
            Log.m("Stopping connection");
            keepOpenTimer.Stop();
            isAvailable = false;
            ComputerInformationJsonObject computerInformation = new ComputerInformationJsonObject();
            computerInformation.available = isAvailable;
            String computerJson = JsonConvert.SerializeObject(computerInformation);
            Log.m(computerJson);
            String toSend = "computer:" + computerJson;
            Byte[] data = Encoding.ASCII.GetBytes(toSend);
            send(data).AsyncWaitHandle.WaitOne();            
            if (client != null)
            {
                try
                {
                    //client.EndReceive(recieveResult, ref remoteEndPoint);
                }
                catch (Exception e)
                {
                    Log.e(e.Message);
                }
                client.Close();
            }
        }
    }
}
