﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrinterHandler
{
    public interface IMessage
    {
        int getID();
        Object getData();
    }
}
