﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace PrinterHandler
{
    public class Log
    {
        private static bool bLogging = false;

        public static void l(String output)
        {
            if (bLogging)
            {
#if DEBUG
                Console.ForegroundColor = ConsoleColor.White;
                try
                {
                    Console.WriteLine(DateTime.Now.ToString() + ": " + output);
                }
                catch(Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Magenta;
                    Console.WriteLine("Logging Error: " + e.Message);
                }
#endif
#if RELEASE
                if (Networking.Connection.isAvailable)
                {
                    String toSend = "log:l:" + output;
                    Byte[] data = Encoding.ASCII.GetBytes(toSend);
                    Networking.Connection.send(data);
                }
#endif
            }
        }

        public static void e(String error)
        {
            if (bLogging)
            {
#if DEBUG
                Console.ForegroundColor = ConsoleColor.Red;
                try
                {
                    Console.WriteLine(DateTime.Now.ToString() + ": " + error);
                }
                catch(Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Magenta;
                    Console.WriteLine("Logging Error: " + e.Message);
                }
#endif
#if RELEASE

                if (Networking.Connection.isAvailable)
                {
                    String toSend = "log:e:" + error;
                    Byte[] data = Encoding.ASCII.GetBytes(toSend);
                    Networking.Connection.send(data);
                }
#endif
            }
        }

        public static void m(String message)
        {
            if(bLogging)
            {
#if DEBUG
                Console.ForegroundColor = ConsoleColor.Yellow;
                try
                {
                    Console.WriteLine(message);
                }
                catch(Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Magenta;
                    Console.WriteLine("Logging Error: " + e.Message);
                }
#endif
#if RELEASE
                if (Networking.Connection.isAvailable)
                {
                    String toSend = "log:m:" + message;
                    Byte[] data = Encoding.ASCII.GetBytes(toSend);
                    Networking.Connection.send(data);
                }
#endif
            }
        }

        public static void initLogging()
        {
            bLogging = true;
        }

        public static void stopLogging()
        {
            bLogging = false;
        }
    }
}
