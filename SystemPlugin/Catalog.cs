﻿using PrinterHandler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemPlugin.Catalog
{
    class ShutdownMessage : IMessage
    {
        private const int ID = 0;
        public ShutdownMessage()
        {

        }

        public int getID()
        {
            return ID;
        }

        public Object getData()
        {
            return null;
        }
    }

    class ShutdownWithMessageMessage : IMessage
    {
        private const int ID = 1;
        private String message;

        public ShutdownWithMessageMessage(String str)
        {
            message = str;
        }

        public int getID()
        {
            return ID;
        }

        public Object getData()
        {
            return message;
        }
    }

    class RestartMessage : IMessage
    {
        private const int ID = 2;
        public RestartMessage()
        {

        }

        public int getID()
        {
            return ID;
        }

        public Object getData()
        {
            return null;
        }
    }

    class RestartWithMessageMessage : IMessage
    {
        private const int ID = 3;
        private String message;

        public RestartWithMessageMessage(String str)
        {
            message = str;
        }

        public int getID()
        {
            return ID;
        }

        public Object getData()
        {
            return message;
        }
    }
}
