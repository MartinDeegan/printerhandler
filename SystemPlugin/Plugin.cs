﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrinterHandler;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace SystemPlugin
{

    public class Plugin : IPlugin
    {
        public static IntPtr WTS_CURRENT_SERVER_HANDLE = IntPtr.Zero;

        private const int
            SHUTDOWN = 0,
            SHUTDOWN_WITH_MESSAGE = 1,
            RESTART = 2,
            RESTART_WITH_MESSAGE = 3;

        public Plugin()
        {

        }

        public void dllEntry(IMessage message)
        {
            switch(message.getID())
            {
                case SHUTDOWN:
                    Process.Start("shutdown", "/s /f /t 0");
                    break;
                case SHUTDOWN_WITH_MESSAGE:
                    Log.l("Shutdown");
                    if (message.getData() != null)
                    {
                        bool result = false;
                        String title = "Emergency Shutdown";
                        int tlen = title.Length;
                        String msg = (String)message.getData();
                        int mlen = msg.Length;
                        int resp = 0;
                        result = WTSSendMessage(WTS_CURRENT_SERVER_HANDLE, WTSGetActiveConsoleSessionId(), title, tlen, msg, mlen, 0, 0, out resp, false);
                        int err = Marshal.GetLastWin32Error();
                        System.Console.WriteLine("result:{0}, errorCode:{1}, response:{2}", result, err, resp);
                    }
                    Process.Start("shutdown", "/s /f /t 0");
                    break;
                case RESTART:
                    Process.Start("shutdown", "/r /f /t 0");
                    break;
                case RESTART_WITH_MESSAGE:
                    Log.l("Restart");
                    if (message.getData() != null)
                    {
                        bool result = false;
                        String title = "Emergency Restart";
                        int tlen = title.Length;
                        String msg = (String)message.getData();
                        int mlen = msg.Length;
                        int resp = 0;
                        result = WTSSendMessage(WTS_CURRENT_SERVER_HANDLE, WTSGetActiveConsoleSessionId(), title, tlen, msg, mlen, 0, 0, out resp, false);
                        int err = Marshal.GetLastWin32Error();
                        System.Console.WriteLine("result:{0}, errorCode:{1}, response:{2}", result, err, resp);
                    }
                    Process.Start("shutdown", "/r /f /t 0");
                    break;
            }
        }

        [DllImport("wtsapi32.dll", SetLastError = true)]
        static extern bool WTSSendMessage(
                    IntPtr hServer,
                    [MarshalAs(UnmanagedType.I4)] int SessionId,
                    String pTitle,
                    [MarshalAs(UnmanagedType.U4)] int TitleLength,
                    String pMessage,
                    [MarshalAs(UnmanagedType.U4)] int MessageLength,
                    [MarshalAs(UnmanagedType.U4)] int Style,
                    [MarshalAs(UnmanagedType.U4)] int Timeout,
                    [MarshalAs(UnmanagedType.U4)] out int pResponse,
                    bool bWait);

        [DllImport("Kernel32.dll", SetLastError = true)]
        static extern int WTSGetActiveConsoleSessionId();
    }
}
